package com.company;
/**
 * Hung Nguyen 013626210
 * Create rings and connect them using coordinates
 */

import javax.swing.*;
import java.awt.*;

/**
 * class to create and combine rings together
 */
public class OlympicRingComponent extends JComponent {
//    public OlympicRingComponent(int x, int y, int r){
//
//    }

    /**
     * create rings with set colors and coordinates then draw them
     * @param g
     */
    @Override
    public void paintComponent(Graphics g){
        //create an instance of graphics2d
        Graphics2D g2 = (Graphics2D) g;
        int x = 75; //center
        int y = 150; //center
        //create blue ring
        OlympicRing ring1 = new OlympicRing( x,y,50,Color.blue);
        //update coordinate by 50
        x +=50;
        //create black ring
        OlympicRing ring2 = new OlympicRing(x,y,50,Color.black);

        x +=50;
        //create red ring
        OlympicRing ring3 = new OlympicRing(x,y,50,Color.red);

        x = 100;
        y= 175;
        //create yellow ring
        OlympicRing ring4 = new OlympicRing(x,y,50,Color.yellow);
        x +=50;
        //create green ring
        OlympicRing ring5 = new OlympicRing(x,y,50,Color.green);
        //draw the rings
        ring1.draw(g2);
        ring2.draw(g2);
        ring3.draw(g2);
        ring4.draw(g2);
        ring5.draw(g2);
    }
}

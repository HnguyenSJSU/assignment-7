package com.company;
/**
 * Hung Nguyen 013626210
 * Create an eclipse with border resizing
 */

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;

public class EclipseComponent extends JComponent {
    /**
     * draw an eclipse
     * @param g
     */
    public void paintComponent(Graphics g){
        Graphics2D g2 = (Graphics2D) g;
        //get width of the frame
        int x = getWidth();
        //get height of the frame
        int y = getHeight();
        //create an eclipse with the width and height
        Ellipse2D.Double head = new Ellipse2D.Double(0,0,x,y);
        g2.setColor(Color.lightGray);
        //fill the shape with set color
        g2.fill(head);
        g2.draw(head);

    }
}

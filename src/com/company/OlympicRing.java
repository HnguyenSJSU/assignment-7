package com.company;

import javax.swing.*;
/**
 * Hung Nguyen 013626210
 * Class to create a ring with set color and coordinates
 */
import java.awt.*;
import java.awt.geom.Ellipse2D;

/**
 * class to create a ring
 */
public class OlympicRing {
    //variables to store set requirements
    private int nXval;
    private int nYval;
    private int nRadius;
    private Color cRing;

    /**
     * Constructor of the OlympicRing class
     * @param x
     * @param y
     * @param r
     * @param color
     */
    public OlympicRing(int x, int y, int r, Color color){
        this.nXval = x;
        this.nYval = y;
        this.nRadius= r;
        this.cRing = color;
    }

    /**
     * create an instance of the ring and draw the ring
     * @param g2
     */
    public void draw(Graphics2D g2){
        //create an instance of Ellipse2d
        Ellipse2D ring = new Ellipse2D.Double(nXval,nYval,nRadius,nRadius);
        //set properties of the ring
        g2.setColor(cRing);
        g2.setStroke(new BasicStroke(5));
        //draw ring
        g2.draw(ring);
    }


}

package com.company;
/**
 * Hung Nguyen 013626210
 * Creates an image of an eclipse using JFrame and Graphics2D
 */

import javax.swing.*;

public class E3_17 {
    public static void main(String[] args) {
        //create a frame
        JFrame frame = new JFrame();
        //set size of the frame
        frame.setSize(300,400);
        frame.setTitle("Eclipse");
        //turn off frame when exit
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //create the eclipse and add onto the frame
        EclipseComponent EclipseComp = new EclipseComponent();
        frame.add(EclipseComp);
        frame.setVisible(true);
    }
}

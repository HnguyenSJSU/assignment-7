package com.company;

import javax.swing.*;

public class E3_24 {

    public static void main(String[] args) {
        //Create a frame with 300 x 400
        JFrame frame = new JFrame();
        frame.setSize(300,400);
        frame.setTitle("Olympic Ring");
        //Exit frame when close
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //create ring
        OlympicRingComponent comp = new OlympicRingComponent();
        frame.add(comp);
        frame.setVisible(true);
    }
}
